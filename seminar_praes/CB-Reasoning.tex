\documentclass{beamer}
\usepackage{beamerthemeshadow}
\usepackage[ngerman]{babel}
\usepackage[utf8x]{inputenc}
\usepackage{color}
\usepackage{graphicx}
\usepackage{amsmath}
\graphicspath{{./gfx/}}
\begin{document}
\title{Consequence-Based Reasoning}
\author{Maciej Janicki}
\frame{\titlepage}
\frame{\frametitle{Inhalt}\tableofcontents}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Einführung}
\subsection{Motivation}
\frame{\frametitle{Motivation}
	\begin{itemize}
		\item \textbf{Klassifikationsproblem:} für gegebene Ontologie $\mathcal{O}$ alle Konzept-Inklusionen
			generieren, die in $\mathcal{O}$ gelten.
		\item Die meisten Beweiser basieren auf Tableau- oder Hypertableau-Methoden.
		\item Mittels dieser Methoden kann man entscheiden, ob eine konkrete Inklusion in $\mathcal{O}$ gilt.
		\item Um das Klassifikationsproblem zu lösen, muss man also alle möglichen Paare von Konzepten
			generieren und für jedes Paar prüfen, ob eine Inklusion gilt.
		\item \textbf{Problem:} für die meisten Paare gilt die Inklusion nicht, es wird also viel
			unnötige Arbeit gemacht.
		\item (Beispiel an der Tafel)
	\end{itemize}
}

\subsection{Problemstellung}
\frame{\frametitle{Problemstellung}
	\textbf{Gesucht:} Eine Menge der Regeln von der Form: 
	\begin{quote}
		Wenn $\mathcal{O} \models \mathcal{A}_1$, $\mathcal{O} \models \mathcal{A}_2$, $\ldots$, dann auch 
		$\mathcal{O} \models \mathcal{B}_1$, $\mathcal{O} \models \mathcal{B}_2$, $\ldots$.
	\end{quote}
	Die Regeln müssen sein:
	\begin{description}
		\item[korrekt] -- sie erzeugen \textbf{nur} Axiome, die wirklich gelten
		\item[vollständig] -- sie erzeugen \textbf{alle} Axiome, die wirklich gelten
	\end{description}
}

\section{Klassifikationsprozedur für $\mathcal{ELH}$}

\subsection{Definition von $\mathcal{ELH}$}
\frame{\frametitle{Definition von $\mathcal{ELH}$}
	Konzepte:
	\begin{itemize}
		\item atomare Konzepte: $A$, top concept $\top$
		\item Konjunktion: $C \sqcap D$
		\item existentielle Restriktion: $\exists{}r{.}C$
	\end{itemize}
	Axiome:
	\begin{itemize}
		\item Konzeptinklusionen: $C \sqsubseteq D$
		\item einfache Rollen-Inklusionen: $r \sqsubseteq s$
	\end{itemize}
	Eigenschaften:
	\begin{itemize}
		\item entscheibar in polynomialer Zeit
	\end{itemize}
}

\subsection{Polaritäten von Konzepten}
\frame{\frametitle{Polaritäten von Konzepten}
	Polaritäten in Axiomen:
	\begin{itemize}
		\item $C$ kommt in $C$ \textbf{positiv} vor
		\item Seien $C_{+}, C_{-}, D_{+}$ Konzepte, $C$ kommt in $C_{+}$ oder in $D_{+}$ positiv
			oder in $C_{-}$ negativ vor.
			\begin{itemize}
				\item $C$ kommt in 
					\textcolor{gray}{$\neg{}C_{-}$}, 
					$C_{+} \sqcap D_{+}$, 
					\textcolor{gray}{$C_{+} \sqcup D_{+}$},
					$\exists{}R{.}C_{+}$, 
					\textcolor{gray}{$\forall{}R.C_{+}$},
					\textcolor{gray}{$\geq{}n S{.}C_{+}$},
					\textcolor{gray}{$\leq{}m S{.}C_{-}$}, 
					$C_{-} \sqsubseteq D_{+}$ \textbf{positiv} vor.
			\end{itemize}
		\item Negativ: analog
		\item (Beispiel an der Tafel)
	\end{itemize}
	Polaritäten in Ontologien:
	\begin{itemize}
		\item $C$ kommt \textbf{positiv} (\textbf{negativ}) in $\mathcal{O}$ vor, wenn
			$C$ in einem Axiom von $\mathcal{O}$ positiv (negativ) vorkommt.
		\item \textbf{Anmerkung:} Es ist möglich, dass ein Konzept in einer Ontologie
			gleichzeitig positiv und negativ vorkommt.
	\end{itemize}
}

\subsection{Strukturelle Transformation}
\frame{\frametitle{Strukturelle Transformation}
	\textbf{Ziel:} Simplifizierung von Axiomen auf eine Weise, die die ``logische Struktur'' behält.\\~\\
	Für jedes Konzept $C$ definieren wir ein Atomares Konzept $A_C$ und die Funktion $st(C)$:
	\begin{itemize}
		\item $st(A) = A$ ; $st(\top) = \top$ ; \textcolor{gray}{$st(\bot) = \bot$}
		\item \textcolor{gray}{$st(\neg C) = \neg A_C$}
		\item $st(C \sqcap D) = A_C \sqcap A_D$ ; \textcolor{gray}{$st(C \sqcup D) = A_C \sqcup A_D$}
		\item $st(\exists{}R{.}C) = \exists{}R{.}A_C$ ; \textcolor{gray}{$st(\forall{}R{.}C) = \forall{}R{.}A_C$}
		\item \textcolor{gray}{$st({\geq{}nR.C}) = {\geq{}nR.A_C}$} ; 
			\textcolor{gray}{$st({\leq{}mR.C}) = {\leq{}mR.A_C}$}
	\end{itemize}
}
\frame{\frametitle{Strukturelle Transformation (Fortsetzung)}
	Die neue Ontologie $\mathcal{O}'$ beinhält die folgenden Konzept-Inklusionen:
	\begin{itemize}
		\item $A_C \sqsubseteq st(C)$ \quad für alle $C$, die in $\mathcal{O}$ positiv vorkommen
		\item $st(C) \sqsubseteq A_C$ \quad für alle $C$, die in $\mathcal{O}$ negativ vorkommen
		\item $A_C \sqsubseteq A_D$ \quad für alle Konzept-Inklusionen $C \sqsubseteq D \in \mathcal{O}$
	\end{itemize}
	~\\ (Beispiel an der Tafel)
}

\subsection{Vervollständigungsregeln}
\frame{\frametitle{Vervollständigungsregeln}
	$$\mbox{\tiny{\textbf{IR1}}} \; \frac{}{A \sqsubseteq A}$$
	$$\mbox{\tiny{\textbf{IR2}}} \; \frac{}{A \sqsubseteq \top}$$
	$$\mbox{\tiny{\textbf{CR1}}} \; 
		\frac{A \sqsubseteq B \;\;\;\; B \sqsubseteq C \in \mathcal{O}}
		{A \sqsubseteq C}$$
	$$\mbox{\tiny{\textbf{CR2}}} \; 
		\frac{A \sqsubseteq B \;\;\;\; A \sqsubseteq C \;\;\;\; B \sqcap C \sqsubseteq D \in \mathcal{O}}
		{A \sqsubseteq D}$$
	$$\mbox{\tiny{\textbf{CR3}}} \; 
		\frac{A \sqsubseteq B \;\;\;\; B \sqsubseteq \exists{}r{.}C \in \mathcal{O}}
		{A \sqsubseteq \exists{}r{.}C}$$
	$$\mbox{\tiny{\textbf{CR4}}} \; 
		\frac{A \sqsubseteq \exists{}r.B \;\;\;\; r \sqsubseteq s \in \mathcal{O}}
		{A \sqsubseteq \exists{}s.B}$$
	$$\mbox{\tiny{\textbf{CR5}}} \; 
		\frac{A \sqsubseteq \exists{}r.B \;\;\;\; B \sqsubseteq C \;\;\;\; \exists{}r.C \sqsubseteq D \in \mathcal{O}}
		{A \sqsubseteq D}$$
}
\frame{\frametitle{Korrektheit und Vollständigkeit der Regeln}
	\textbf{Satz:} Die Regeln \textbf{\small{IR1-CR5}} sind korrekt und vollständig für die Logik $\mathcal{ELH}$,
		d. h. $\mathcal{O} \models C \sqsubseteq D \Leftrightarrow C \sqsubseteq D \in \mathcal{O}'$. \\
		\small{($\mathcal{O}'$ -- nach struktureller Transformation und Anwendung von Regeln)} \\~\\
	\textbf{Beweis (Skizze):}
	\begin{itemize}
		\item $\Leftarrow$: Die Regeln erzeugen nur logische Konsequenzen von Axiomen.
		\item $\Rightarrow$: Nehme an, dass $C \sqsubseteq D \notin \mathcal{O}'$. Dann wird eine solche
			Interpretation $\mathcal{I}$ konstruiert, dass $C^{\mathcal{I}} \nsubseteq D^{\mathcal{I}}$.
	\end{itemize}
}


\section{Erweiterung auf Horn-$\mathcal{SHIQ}$}
\subsection{Definition von Horn-$\mathcal{SHIQ}$}
\frame{\frametitle{Die Beschreibungslogik $\mathcal{SHIQ}$}
	Konzepte:
	\begin{itemize}
		\item $A$, $\top$, $\bot$
		\item $\neg C$
		\item $C \sqcap D$, $C \sqcup D$
		\item $\exists{}r{.}C$, $\forall{}r.C$
		\item ${\geq nS.C}$, ${\leq m S.C}$ \quad $S$ - \emph{einfach} (hat keine transitive Subrolle)
	\end{itemize}
	Axiome:
	\begin{itemize}
		\item Konzeptinklusionen: $C \sqsubseteq D$
		\item Rollen-Inklusionen: $R_1 \sqsubseteq R_2$
		\item Rollen-Transitivität: $Tra(R)$
	\end{itemize}
	Dazu noch: inverte Rollen
}
\frame{\frametitle{Horn-$\mathcal{SHIQ}$}
	Eine $\mathcal{SHIQ}$-Ontologie $\mathcal{O}$ ist \emph{Horn}, wenn:
	\begin{itemize}
		\item \textbf{kein} Konzept von der Form $C \sqcup D$ oder ${\leq mR.C}$ mit $m > 1$
			in $\mathcal{O}$ \textbf{positiv} vorkommt
		\item \textbf{kein} Konzept von der Form $\neg C$, $\forall{}R.C$, ${\geq nR.C}$ mit $n > 1$
			oder ${\leq m R.C}$ in $\mathcal{O}$ \textbf{negativ} vorkommt
	\end{itemize}
}
\frame{\frametitle{Universale Restriktionen}
	Universale Restriktionen können mittels existentieller Restriktionen und inverter Rollen
	ausgedrückt werden:
	$$ A \sqsubseteq \forall{}R.B \Leftrightarrow \exists{}R^{-}.A \sqsubseteq B$$
}

\subsection{Neue Schlussfolgerungen}
\frame{\frametitle{Neue Schlussfolgerungen}
	Für Horn-$\mathcal{SHIQ}$ Ontologien sind neue Schlussfolgerungen möglich, z. B.:
	$$\mbox{} \; 
		\frac{A \sqsubseteq \exists{}R.B \;\;\;\; C \sqsubseteq \forall{}R.D}
		{A \sqcap C \sqsubseteq \exists{}R.(B \sqcap D)}$$
	\textbf{Problem:}
	\begin{itemize}
		\item in den Regeln für $\mathcal{ELH}$, auf der linken Seite von erzeugten Axiomen steht immer
			\emph{ein} atomares Konzept
		\item bei Horn-$\mathcal{SHIQ}$, auf der linken Seite kann eine Konjunktion von atomaren
			Konzepten stehen (Anzahl: höchstens exponentiell)
	\end{itemize}
	Deswegen: Anzahl von solchen Axiomen nicht mehr polynomial, sondern exponentiell beschränkt.
}

\subsection{Vorbearbeitung}
\frame{\frametitle{Vorbearbeitung}
	\textbf{Ziel:} Transformation $\mathcal{O} \rightarrow \mathcal{O}'$, wobei:
		\begin{itemize}
		\item alle Axiome von $\mathcal{O}'$ haben die Form:
			(n1)~$\rotatebox[origin=c]{180}{$\bigsqcup$} A_i \sqsubseteq C$ oder (n2)~$R_1 \sqsubseteq R_2$,
			wobei 
		\item $C$ ist ein einfaches Konzept, d. h. $C$ hat die Form
			$\bot$, $A$, $\exists{}R.A$, $\forall{}R.A$ oder ${\leq 1 S.A}$.
		\end{itemize}
	\textbf{Verfahren:}
	\begin{itemize}
		\item strukturelle Transformation
		\item Transformation von Konzept-Inklusionen von der Form: 
			$A \sqsubseteq st(C_{+})$, $st(C_{-}) \sqsubseteq A$ in die Form (n1)
		\item Elimination von Transitivität
	\end{itemize}
}

\subsection{Vervollständigungsregeln}
\frame{\frametitle{Vervollständigungsregeln}
	\tiny{
	\begin{displaymath}
		\mbox{\textbf{\tiny{I1}}}
		\frac{}{M \sqcap A \sqsubseteq A}
		\;\;\;\;
		\mbox{\textbf{\tiny{I2}}}
		\frac{}{M \sqsubseteq \top}
	\end{displaymath}
	\begin{displaymath}
		\mbox{\textbf{\tiny{R1}}}
		\frac{
			\begin{array}{ll}
				M \sqsubseteq A_i & \rotatebox[origin=c]{180}{$\bigsqcup$} A_i \sqsubseteq C \in \mathcal{O}
			\end{array}
		}
		{
			M \sqsubseteq C
		}
	\end{displaymath}
	\begin{displaymath}
		\mbox{\textbf{\tiny{R2}}}
		\frac{
			\begin{array}{ll}
				M \sqsubseteq \exists{}R.N & N \sqsubseteq \bot
			\end{array}
		}
		{
			M \sqsubseteq \bot
		}
	\end{displaymath}
	\begin{displaymath}
		\mbox{\textbf{\tiny{R3}}}
		\frac{
			\begin{array}{lll}
				M \sqsubseteq \exists{}R_1.N & M \sqsubseteq \forall{}R_2.A
				& R_1 \sqsubseteq_{\mathcal{O}} R_2
			\end{array}
		}
		{
			M \sqsubseteq \exists{}R_1.(N \sqcap A)
		}
	\end{displaymath}
	\begin{displaymath}
		\mbox{\textbf{\tiny{R4}}}
		\frac{
			\begin{array}{lll}
				M \sqsubseteq \exists{}R_1.N & N \sqsubseteq \forall{}R_2.A
				& R_1 \sqsubseteq_{\mathcal{O}} R_2^{-}
			\end{array}
		}
		{
			M \sqsubseteq A
		}
	\end{displaymath}
	\begin{displaymath}
		\mbox{\textbf{\tiny{R5}}}
		\frac{
			\begin{array}{lll}
				M \sqsubseteq \exists{}R_1.N_1 & N_1 \sqsubseteq B & R_1 \sqsubseteq_{\mathcal{O}} S \\
				M \sqsubseteq \exists{}R_2.N_2 & N_2 \sqsubseteq B & R_2 \sqsubseteq_{\mathcal{O}} S \\
				M \sqsubseteq {\leq 1 S.B}
			\end{array}
		}
		{
			M \sqsubseteq \exists{}R_1.(N_1 \sqcap N_2)
		}
	\end{displaymath}
	\begin{displaymath}
		\mbox{\textbf{\tiny{R6}}}
		\frac{
			\begin{array}{lll}
				M \sqsubseteq \exists{}R_1.N_1 & N_1 \sqsubseteq \exists{}R_2.(N_2 \sqcap A)
				& R_1 \sqsubseteq_{\mathcal{O}} S^{-} \\
				M \sqsubseteq B & N_2 \sqcap A \sqsubseteq B & R_2 \sqsubseteq_{\mathcal{O}} S \\
				M \sqsubseteq {\leq 1 S.B}
			\end{array}
		}
		{
			\begin{array}{ll}
				M \sqsubseteq A & M \sqsubseteq \exists{}R^{-}_2.N_1
			\end{array}
		}
	\end{displaymath}
	}
}

\subsection{Korrektheit und Vollständigkeit}

\section{Evaluation}
\frame{\frametitle{Evaluation}
	\textbf{Genommene Ontologien:}
	\begin{itemize}
		\item Gene Ontology (GO), National Cancer Instutute Thesaurus (NCI) -- große Ontologien
			mit wenig Abhängigkeiten
		\item Snomed -- sehr große, einfache Ontologie
		\item zwei Versionen von Galen: GalenA (frühe, kleine Version), GalenB (aktuelle, ziemlich große Version)
			-- viele existentielle Restriktionen, die für Modell-basierte Beweiser besonders schwer sind
		\item GalenA-, GalenB- -- aus GalenA, GalenB bekommen durch Elimination von funktionalen Restriktionen
			und inverten Rollen
	\end{itemize}
	\textbf{Vergleich mit anderen Beweisern:}
	\begin{itemize}
		\item Tableau-basiert: FaCT++, Pellet, HermiT
		\item Vervollständigung-basiert: CEL
	\end{itemize}
}

\frame{\frametitle{Evaluation -- Ergebnisse}
	\begin{table}
	\begin{tabular}{l|r|r|r|r|r}
			& FaCT++ 	& Pellet	& HermiT 	& CEL 			& CB		\\
	\hline
	GO		& 15.24		& 72.02		& 199.52	& 1.84			& 1.17		\\
	NCI		& 6.05		& 26.47		& 169.47	& 5.76			& 3.57		\\
	GalenA-	& 3166.74	& 133.25	& 91.98		& 3.27			& 0.26		\\
	GalenA	& 465.35	& ---		& 45.72		& \emph{n/a}	& 0.32		\\
	GalenB-	& ---		& ---		& ---		& 189.12		& 4.07		\\
	GalenB	& ---		& ---		& ---		& \emph{n/a}	& 9.58		\\
	Snomed	& 650.37	& ---		& ---		& 1185.70		& 49.44		\\
	\end{tabular}
	\caption{Vergleich von Klassifikationszeiten (in Sekunden): ``---''~bedeutet, dass
		der Beweiser innerhalb von einer Stunde kein Resultat gegeben hat; \emph{n/a}, dass
		der Test nicht ausgeführt werden konnte.}
	\end{table}
}

\end{document}
